# RQT Cristal Map View

## Dependencies 

This package was validated on ROS Noetic under Ubuntu 20.04. Please install a valid ROS Noetic environment http://wiki.ros.org/noetic/Installation/Ubuntu

It depends on the Qt Location features and QML

```
sudo apt install qml-module-qtlocation qml-module-qtquick* qml-module-qtpositioning 
```

## Building 


In the workspace folder, use catkin_make to build the package.

## Running 

Clone the repository in the src folder of your ROS workspace: 

```
cd catkin_ws/src
git clone https://gitlab.cristal.univ-lille.fr/open-pretil/rqt-cristal-map-view
```

In the workspace folder : 
``` 
source devel/setup.bash
rqt
```

In the rqt window, select the menu Plugins -> rqt plugins -> GPS Map View 

Click on the refresh icon to make the data sources available. 

## Design

<img src="http://yuml.me/diagram/scruffy/class/[QWidget]^[GpsMapViewWidget], [GpsMapViewWidget]++0..n->[GnssMarker], [GpsMapView]->[GpsMapViewWidget],[GpsMapViewWidget]++0..n->[QCheckBox], [GnssMarker|-longitude:double;-latitude:double;timer:QTimer|track();getLongitude();getLatitude();], [QCheckBox]-.-[note: A chaque activation/désactivation, mets à jour la liste de GnssMarker]"/>

