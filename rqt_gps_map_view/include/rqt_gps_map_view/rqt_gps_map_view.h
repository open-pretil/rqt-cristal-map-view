#ifndef RQT_GPS_MAP_VIEW
#define RQT_GPS_MAP_VIEW

#include <rqt_gui_cpp/plugin.h>
#include <std_msgs/String.h>

#include <rqt_gps_map_view/rqt_gps_map_view_widget.h>

namespace rqt_gps_map_view {

class GpsMapView : public rqt_gui_cpp::Plugin
{
public:
    GpsMapView();

    void initPlugin(qt_gui_cpp::PluginContext& context) override;
    void shutdownPlugin() override;

    void saveSettings(qt_gui_cpp::Settings& plugin_settings, qt_gui_cpp::Settings& instance_settings) const override;
    void restoreSettings(const qt_gui_cpp::Settings& plugin_settings, const qt_gui_cpp::Settings& instance_settings) override;

private:
    GpsMapViewWidget *widget = nullptr;
};

}

#endif // RQT_GPS_MAP_VIEW

