/*
Created on 04 April 2022

Author : Boulongne Corentin

Brief : class to subscribe to ROS GPS data and track markers with QML.

LICENCE : to define

*/


#ifndef GNSSMARKER_H
#define GNSSMARKER_H

#include <ros/ros.h>
#include <sensor_msgs/NavSatFix.h>
#include <std_msgs/String.h>
#include <ros/master.h>
#include <string>
#include <boost/bind.hpp>
#include <qt5/QtCore/QList>
#include <qt5/QtCore/QTimer>
#include <QQuickItem>
#include <QString>

class GnssMarker: public QObject
{
    Q_OBJECT

public:

               GnssMarker(); 
               GnssMarker(std::string topicToSub);
               void subScribeTo(std::string topicName);
               void getData(const sensor_msgs::NavSatFix& data);
               QString getName();
            
               void sendObjects(QQuickItem* itm, QString markerName_); 

               double getLongitude();
               double getLatitude();
               ~GnssMarker();

public slots:

                void track();


private:
            
               ros::NodeHandle nh_;
               ros::Subscriber subNavSatFix;
               QQuickItem* item;
               QString marker_name;
               QTimer * timer;
               


protected:

               double longitude;
               double latitude;

};












#endif // GNSSMARKER_H
