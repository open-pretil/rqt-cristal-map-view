/*
Created on 04 April 2022

Author : Boulongne Corentin

Brief : User Interface to display GPS sources on a map and track them.

LICENCE : to define

*/


#ifndef RQT_GPS_MAP_VIEW_WIDGET
#define RQT_GPS_MAP_VIEW_WIDGET

#include <QWidget>
#include <sensor_msgs/NavSatFix.h>
#include <ros/ros.h>
#include <ros/message_event.h>
#include <QTimer>
#include <boost/bind.hpp>
#include <ros/master.h>
#include <QQmlEngine>
#include <QQuickItem>
#include <QCheckBox>
#include <QListWidget>
#include <QMessageBox>
#include <QVector>

#include "gnssMarker.h"

namespace Ui {
class GpsMapViewWidget;
}

class GpsMapViewWidget : public QWidget
{
    Q_OBJECT

public:
    explicit GpsMapViewWidget(QWidget *parent = nullptr);
    ~GpsMapViewWidget();

public slots:
    

    void updateTopicList();

    void afficherPosition(QString latitude, QString longitude);
    void afficherInformations(QString adresse, QString coordonnee);

    void getInfo(const ros::MessageEvent<sensor_msgs::NavSatFix>& event);
 

private slots:
    void on_refreshButton_clicked();

    void on_checkBoxClicked(int state);


private:
    Ui::GpsMapViewWidget *ui;

    QList<GnssMarker *> gnssMarkerList;

    QVector<QCheckBox*> checkBoxes;

    int mID = -1;
    QString markerName_ = "";



};

#endif // RQT_GPS_MAP_VIEW_WIDGET
