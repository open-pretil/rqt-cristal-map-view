/*
Created on 04 April 2022

Author : Boulongne Corentin

Brief : class to subscribe to ROS GPS data and track markers with QML.

LICENCE : to define

*/

#include "gnssMarker.h"
#include <iostream>

GnssMarker::GnssMarker()
{
    
}


GnssMarker::GnssMarker(std::string topicToSub)
{

//    ros::init(argc, argv, "GNSS-MARKER Node");

    this->timer = new QTimer();

    timer->setInterval(20);
    timer->start();
    subNavSatFix = nh_.subscribe(topicToSub, 100,  &GnssMarker::getData, this);
    ROS_INFO(" %s", topicToSub.c_str());

    //ros::spin();

}



void GnssMarker::subScribeTo(std::string topicName)
{

    subNavSatFix = nh_.subscribe(topicName, 100,  &GnssMarker::getData, this);
    ROS_INFO(" %s", topicName.c_str());

}


void GnssMarker::getData(const sensor_msgs::NavSatFix& data)
{

    longitude = data.longitude;
    latitude = data.latitude;

   // std::cout << data.header.frame_id << std::endl << data.longitude << std::endl << data.latitude << std::endl;

}


double GnssMarker::getLatitude()
{
    return latitude;
}

double GnssMarker::getLongitude()
{
    return longitude;
}

QString GnssMarker::getName()
{
    return marker_name;
}


void GnssMarker::sendObjects(QQuickItem* itm, QString markerNames)
{

    item = itm;

    marker_name = markerNames;

    QTimer::connect(timer, SIGNAL(timeout()), this, SLOT(track()));

}



void GnssMarker::track()
{

    QObject *obj = item->findChild<QObject *>("locationList");

    QList<QQuickItem*> mapItem = item->findChildren<QQuickItem*>("mapItemView");
    auto test_2 = mapItem.at(0);

    auto mapItemChild = test_2->childItems();


    int nb_markers = mapItemChild.count(); //return nb of markers

    QStringList listOfMarkers;

    for(int j = 0; j < nb_markers; j++)
    {
       listOfMarkers << mapItemChild.at(j)->objectName();
    }

    int idx = listOfMarkers.indexOf(marker_name);

    auto marker_id = mapItemChild.at(idx); // throw error if index is out of range -> app crash.

    QVariant latitude_ = latitude;
    QVariant longitude_ = longitude;
  
    if(marker_id != nullptr)
    {

        QMetaObject::invokeMethod(marker_id, "track", Q_ARG(QVariant, latitude_), Q_ARG(QVariant, longitude_));
  //     std::cout << "track function called!" << std::endl;

    }

}

GnssMarker::~GnssMarker()
{

    QTimer::disconnect(timer, SIGNAL(timeout()), this, SLOT(track()));

    delete timer;

}
