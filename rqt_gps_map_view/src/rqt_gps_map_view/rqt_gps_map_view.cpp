// License 
// Date 
// Author 
// Description 


#include <rqt_gps_map_view/rqt_gps_map_view.h>

#include <pluginlib/class_list_macros.h>
#include <ros/master.h>

namespace rqt_gps_map_view {


// constructor 
GpsMapView::GpsMapView() :
    rqt_gui_cpp::Plugin()
{
    setObjectName("GpsMapView");
}

void GpsMapView::initPlugin(qt_gui_cpp::PluginContext &context)
{
    widget = new GpsMapViewWidget();
    context.addWidget(widget);
}

void GpsMapView::shutdownPlugin()
{

}

void GpsMapView::saveSettings(qt_gui_cpp::Settings &plugin_settings, qt_gui_cpp::Settings &instance_settings) const
{

}

void GpsMapView::restoreSettings(const qt_gui_cpp::Settings &plugin_settings, const qt_gui_cpp::Settings &instance_settings)
{

}

} // end namespace rqt_gps_map_view

PLUGINLIB_EXPORT_CLASS(rqt_gps_map_view::GpsMapView, rqt_gui_cpp::Plugin)
