/*
Created on 04 April 2022

Author : Boulongne Corentin

Brief : User Interface to display GPS sources on a map and track them.

LICENCE : to define

*/


#include <rqt_gps_map_view/rqt_gps_map_view_widget.h>
#include "ui_rqt_gps_map_view_widget.h"

#include <QQmlEngine>
#include <QQuickItem>
#include <QCheckBox>
#include <QListWidget>
#include <iostream>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QDebug>
#include <QVector>
#include <QVBoxLayout>



GpsMapViewWidget::GpsMapViewWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::GpsMapViewWidget)
{
    ui->setupUi(this);

    QQuickItem *item = ui->quickWidget->rootObject();

    QObject *objectMouseArea = item->findChild<QObject *>("mouseArea");

    if(objectMouseArea != nullptr)
    {
        connect(objectMouseArea, SIGNAL(afficherPosition(QString,QString)), this, SLOT(afficherPosition(QString,QString)));
    }

    QObject *objectGeocodage = item->findChild<QObject *>("geocodage");

    if(objectGeocodage != nullptr)
    {
       connect(objectGeocodage, SIGNAL(afficherInformations(QString,QString)), this, SLOT(afficherInformations(QString,QString)));
    }

    updateTopicList();


}

GpsMapViewWidget::~GpsMapViewWidget()
{
    foreach(QCheckBox* cb, checkBoxes)
    {
        cb->setChecked(false);
    }

    for (int idx = 0 ; idx < gnssMarkerList.count(); ++idx)
    {
        delete gnssMarkerList.at(idx);
        gnssMarkerList.removeAt(idx);
    }
    delete ui;
}


void GpsMapViewWidget::updateTopicList()
{

    QStringList topicsNames;
    ui->topicList->clear();
    ui->plainTextEdit->clear();
    topicsNames.clear();
    checkBoxes.clear();
    
    ros::master::V_TopicInfo topics;
    ros::master::getTopics(topics);

    for(ros::master::V_TopicInfo::iterator it = topics.begin(); it != topics.end(); it++)
    {
        const ros::master::TopicInfo& topicInfo = *it;


        if(topicInfo.datatype == "sensor_msgs/NavSatFix")
        {

            ui->topicList->addItem(topicInfo.name.c_str());
            ui->plainTextEdit->appendPlainText(topicInfo.name.c_str());
            ui->plainTextEdit->textCursor().insertText(" - ");
            ui->plainTextEdit->textCursor().insertText(topicInfo.datatype.c_str());

            topicsNames << topicInfo.name.c_str();

        }

    }

    if(topicsNames.isEmpty() == true)
    {

        QMessageBox msgBox;
        msgBox.setWindowTitle("Error while retreiving topics");
        msgBox.setIcon(QMessageBox::Warning);
        msgBox.setText("It appears that no topics where available, make sure at least one sensor_msgs/NavSatFix topic is being published.");
        msgBox.exec();

    }else
    {

        QVBoxLayout *vbox = new QVBoxLayout;

        foreach(QString filt, topicsNames){

            QCheckBox *checkbox = new QCheckBox(filt, this);

            checkBoxes.push_back(checkbox);
            checkbox->setChecked(false);

            vbox->addWidget(checkbox);

            ui->dataSourcesGB->setLayout(vbox);

            connect(checkbox, &QCheckBox::stateChanged, this, &GpsMapViewWidget::on_checkBoxClicked);

        }        

    }

    //ui->dataSourcesGB->setLayout(vbox);

}

// slot 
void GpsMapViewWidget::on_checkBoxClicked(int state)
{

    auto cb = qobject_cast<QCheckBox*>(sender());

    QQuickItem *item = ui->quickWidget->rootObject();
    QObject *obj = item->findChild<QObject *>("locationList");

    /*QList<QQuickItem*> mapItem = item->findChildren<QQuickItem*>("mapItemView");
    auto test_2 = mapItem.at(0);

    auto mapItemChild = test_2->childItems(); // list of markers 0 to N -> name is markerX / X is the id
*/
    // int nb_markers = mapItemChild.count(); //return nb of markers

    QStringList list;

    for(int j = 0; j < gnssMarkerList.count(); j++)
    {
       // list << mapItemChild.at(j)->objectName();
       list << gnssMarkerList.at(j)->getName();
    }

    if(state == 2) // -> checkbox is checked
    {

        QString chboxName = cb->text();

        int mID = gnssMarkerList.count();

        gnssMarkerList << new GnssMarker(chboxName.toStdString());

        chboxName.replace(QString("/"), QString("_"));

        QVariant latitude = 0.0; // create point on center
        QVariant longitude = 0.0;
        QVariant markerName = chboxName;

        markerName_ = "marker"+chboxName;

        //std::cout << markerName_.toStdString() << std::endl;
        
        QMetaObject::invokeMethod(obj, "addMarker", Q_ARG(QVariant, mID), Q_ARG(QVariant, markerName), Q_ARG(QVariant, latitude), Q_ARG(QVariant, longitude));

        gnssMarkerList.at(mID)->sendObjects(item, markerName_);
      
      //  std::cout << "this checkbox (" << cb->text().toStdString() << ") checked." << std::endl;

    }
    else if(state != 2)
    {
        QString chboxName = cb->text();

        chboxName.replace(QString("/"), QString("_"));

        int idx = list.indexOf("marker"+chboxName);

        QVariant indexToRemove = idx;

        QMetaObject::invokeMethod(obj, "removeMarker", Q_ARG(QVariant, indexToRemove));  // removing correstponding marker with the topic name

        delete gnssMarkerList.at(idx);
        gnssMarkerList.removeAt(idx);

        std::cout << "this checkbox (" << cb->text().toStdString() << ") unchecked." << std::endl;

    }

    //std::cout << "checkbox " << cb->text().toStdString() << " state is : " << state << std::endl;


}

void GpsMapViewWidget::afficherPosition(QString latitude, QString longitude)
{

    ui->positionLatitude->setText(QString::fromUtf8("%1").arg(latitude.toDouble(), 0, 'f', 5));
    ui->positionLongitude->setText(QString::fromUtf8("%1").arg(longitude.toDouble(), 0, 'f', 5));
    //ui->editLatitude->setText(QString::fromUtf8("%1").arg(latitude.toDouble(), 0, 'f', 5));
    //ui->editLongitude->setText(QString::fromUtf8("%1").arg(longitude.toDouble(), 0, 'f', 5));
}

void GpsMapViewWidget::afficherInformations(QString adresse, QString coordonnee)
{
    ui->labelInformations->setText("Informations : " + adresse);
}


void GpsMapViewWidget::getInfo(const ros::MessageEvent<sensor_msgs::NavSatFix> &event)
{
    const std::string& publisher_name = event.getPublisherName();
    const ros::M_string& header = event.getConnectionHeader();
    ros::Time receipt_time = event.getReceiptTime();

    const sensor_msgs::NavSatFixConstPtr& msg = event.getMessage();

    //std::cout << "[DEBUG] " << publisher_name << std::endl;

}

void GpsMapViewWidget::on_refreshButton_clicked()
{
    updateTopicList();
}
