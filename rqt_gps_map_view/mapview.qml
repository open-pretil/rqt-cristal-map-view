import QtQuick 2.0
import QtQuick.Controls 2.2
import QtLocation 5.3
import QtPositioning 5.0

Item {
    property variant coordonneeCentre: QtPositioning.coordinate(50.607511559590165, 3.1378989726334985) // Parking polytech lille / ESPRIT


    ListModel{
            id: locationModel
            objectName: "locationList"
            //ListElement {mID: 0; Mname: "marker0"; lat: 36.688089; lon: 3.034564; color: "orange"; radius:600}
            /*ListElement {mID: 1; Mname: "marker1"; lat: 36.692111; lon: 3.028278; color: "green"; radius:400}
            ListElement {mID: 2; Mname: "marker2"; lat: 36.693239; lon: 3.029731; color: "orange"; radius:500}
            ListElement {mID: 3; Mname: "marker3"; lat: 36.693444; lon: 3.029972; color: "red"; radius:300}
            ListElement {mID: 4; Mname: "marker4"; lat: 36.693472; lon: 3.042250; color: "green"; radius:200}
            ListElement {mID: 5; Mname: "marker5"; lat: 36.691561; lon: 3.043039; color: "orange"; radius:450}
            ListElement {mID: 6; Mname: "marker6"; lat: 36.691250; lon: 3.042861; color: "red"; radius:200}*/

            function addMarker(markerID, markerName, lati, longi)
            {

                locationModel.append({mID: markerID, Mname: markerName, lat: lati, lon: longi})

            }
            function removeMarker(i)
            {

                locationModel.remove(i);

            }
        }


    Plugin
    {
        id: mapPlugin
        locales: "fr_FR"
        name: "osm" // OpenStreetMap
        PluginParameter { name: "osm.geocoding.host"; value: "https://nominatim.openstreetmap.org" }
    }

    Map
    {
        id: map
        anchors.fill: parent
        plugin: mapPlugin
        center: coordonneeCentre
        minimumZoomLevel: 5
        zoomLevel: 15
        gesture.enabled: true
        /*MapQuickItem
        {
            id: indicateurPosition
            objectName: "indicateurPosition"
            coordinate: coordonneeCentre
            sourceItem: Rectangle {width: 12; height: 12; color: "red"; border.width: 2; border.color: "red"; smooth: true; radius: 15;}
            function aller(latitude, longitude)
            {
                console.log("aller")
                //map.clearMapItems();
                indicateurPosition.coordinate.latitude = latitude;
                indicateurPosition.coordinate.longitude = longitude;
                map.addMapItem(indicateurPosition);
                map.update();
            }
        }*/

        /*MapQuickItem
        {
            id: indicateurPosition2
            objectName: "indicateurPosition2"
            coordinate: coordonneeCentre
            sourceItem: Rectangle {width: 12; height: 12; color: "blue"; border.width: 2; border.color: "blue"; smooth: true; radius: 15;}
            function poseublx(latitude, longitude)
            {
                console.log("aller")
               // map.clearMapItems();
                indicateurPosition2.coordinate.latitude = latitude;
                indicateurPosition2.coordinate.longitude = longitude;
                map.addMapItem(indicateurPosition2);
                map.update();
            }
        }*/

        MapItemView {
            model: locationModel
            objectName: "mapItemView"
            delegate: MapQuickItem {

                id: mID
                objectName: "marker" + Mname
                coordinate: QtPositioning.coordinate(lat, lon)
                //anchorPoint: Qt.point(10,30) // les coordonnées locale de l'icon a centées sur les coordonnées géographique
                property var randomColor: Qt.rgba(Math.random(),Math.random(),Math.random(),1)
                sourceItem: Rectangle {width: 12; height: 12; color: randomColor; border.width: 2; border.color: randomColor; smooth: true; radius: 15;}
                function track(latitude, longitude)
                {
                    console.log("poseUBX")
                   // map.clearMapItems();
                    mID.coordinate.latitude = latitude;
                    mID.coordinate.longitude = longitude;
                    map.addMapItem(mID);
                    map.update();

                }


                }
                Component.onCompleted: map.update()

            }

    }


    Button
    {
        id: zoomPlus
        text: 'Zoom +'
        width: implicitWidth * 1.5
        height: implicitHeight * 1.5
        anchors {
            left: parent.left
            leftMargin: 4
            bottom: parent.bottom
            bottomMargin: 30
        }
        font.bold: true
        background: Rectangle {
            color: "lightgray"
            opacity: 0.5
            radius: 10
        }
        onClicked: map.zoomLevel++
    }

    Button
    {
        id: zoomMoins
        text: 'Zoom -'
        width: implicitWidth * 1.5
        height: implicitHeight * 1.5
        anchors {
            left: zoomPlus.right
            leftMargin: 10
            bottom: parent.bottom
            bottomMargin: 30
        }
        font.bold: true
        background: Rectangle {
            color: "lightgray"
            opacity: 0.5
            radius: 10
        }
        onClicked: map.zoomLevel--
    }

    Button
    {
        id: bouttonCentre
        text: "Centrer"
        width: implicitWidth * 1.5
        height: implicitHeight * 1.5
        anchors {
            left: zoomMoins.right
            leftMargin: 10
            bottom: parent.bottom
            bottomMargin: 30
        }
        font.bold: true
        background: Rectangle {
            color: "lightgray"
            opacity: 0.5
            radius: 10
        }
        onClicked: {
            map.clearMapItems();
            indicateurPosition.coordinate = coordonneeCentre;
            map.addMapItem(indicateurPosition);
            map.center = coordonneeCentre
            map.update();
        }
    }

    GeocodeModel
    {
        id: geocodage
        objectName: "geocodage"
        plugin: mapPlugin
        signal afficherInformations(string adresse, string coordonnee)
        onLocationsChanged:
        {
            if(error)
                console.log("Erreur GeocodeModel : " + error + " - " + errorString)
            if (count>=1)
            {
                console.log("Adresse : " + get(0).address.text + "\n" + get(0).coordinate)
                geocodage.afficherInformations(get(0).address.text, get(0).coordinate)
            }
        }
     }



   /* MouseArea
    {
        id: mouseArea
        objectName: "mouseArea"
        width: parent.width
        height: parent.height - (zoomMoins.height * 1.5)
        //anchors.fill: map
        hoverEnabled: true
        property var coordonnee: map.toCoordinate(Qt.point(mouseX, mouseY))
        signal afficherPosition(string latitude, string longitude)
        onClicked:
        {
            console.log(coordonnee.latitude + " " + coordonnee.longitude)
            geocodage.query = coordonnee
            geocodage.update()
            mouseArea.afficherPosition(coordonnee.latitude, coordonnee.longitude)

        }

    }*/
}
